Pre-requisites
--------------

- MySQL
- Python 2.7
- Python Package manager - [pip](http://www.pip-installer.org/)

Setting up
----------

- Create `config.py` and copy the contents of `sample_config.py` and edit
  values appropriately.

- Run `$ pip install flask Flask-SQLAlchemy oursql requests` to install
  dependencies.

- Make sure the database mentioned in the `DB_URI` (in `config.py`) is created.

- Run `$ python dbsetup.py` to create the tables.

Running
-------

Run `$ python server.py` and navigate the to URL in your browser.
