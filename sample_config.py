# database URI
DB_URI = 'mysql+oursql://user:pass@localhost/musiclab?charset=utf8&use_unicode=0'

# moz persona verifier endpoint - leave it as it is
MOZ_PERSONA_VERIFIER = 'https://verifier.login.persona.org/verify'

# the URL at which the app is runnning
APP_URL = 'http://localhost:5000'

# some long random key to set secure sessions..
SECRET_KEY = '7ygwdhas7otyqhdugcyofr7tlyiws'
