# -*- coding: utf-8 -*-

from flask import Flask, render_template, make_response, request, session
import json
import requests

import config
from user import User, db


# function to create the Flask app
# useful in external scripts
def create_app():
    app = Flask(__name__)

    app.config['SQLALCHEMY_DATABASE_URI'] = config.DB_URI

    db.init_app(app)
    app.db = db

    return app


# create our app
app = create_app()
app.secret_key = config.SECRET_KEY


# the root URL
@app.route('/')
def index():
    return render_template('index.html')


@app.route('/login', methods=['POST'])
def user_login():
    response = make_response()

    if 'assertion' not in request.form:
        response.status_code = 400
        return response

    data = {'assertion': request.form['assertion'], 'audience':
            config.APP_URL}

    resp = requests.post(config.MOZ_PERSONA_VERIFIER, data=data, verify=True)

    print 'Response code from MOZ_PERSONA_VERIFIER %s' % resp.status_code
    print 'Response body: %s' % resp.json()

    if resp.ok:
        verified_data = json.loads(resp.content)
        if verified_data['status'] == 'okay':
            user_email = verified_data['email']
            # check if this user exists in our system
            current_user = User.query.filter_by(email=user_email).first()
            # user doesn't exist; create her
            if current_user is None:
                print 'user with email %s doesn\'t exist' % user_email
                print 'creating new user: %s' % user_email

                # get the email_id and use it as a default username
                temp_username = user_email.split('@')[0]
                new_user = User(temp_username, user_email)
                new_user.persist()
                current_user = new_user

            print 'logging in user with email %s' % user_email
            session['email'] = current_user.email

            response.status_code = 200
            response.data = {'email': user_email}
            return response

    response.status_code = 500
    return response


@app.route('/logout', methods=['POST'])
def user_logout():
    response = make_response()

    if 'email' in session:
        print 'logging out user %s' % session['email']
        session.pop('email')

    response.status_code = 200
    return response


if __name__ == "__main__":
    app.run(debug=True)
