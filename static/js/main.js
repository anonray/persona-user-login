(function(M) {

  var Persona = function(logged_in_user, login_url, logout_url) {
    navigator.id.watch({
      loggedInUser: logged_in_user,
      //when an user logs in
      onlogin: function(assertion) {
        //verify assertion and login the user
        $.ajax({
          type: 'POST',
          url: login_url,
          data: {assertion: assertion},
          success: function(data) {
            console.log('successful login..', data);
            window.location.reload();
          },
          error: function() {
            navigator.id.logout();
          }
        });
      },
      onlogout: function() {
        $.ajax({
          type: 'POST',
          url: logout_url,
          success: function() {
            window.location.reload();
            console.log('logged out');
          },
          error: function() {
          }
        });
      }
    });

    return this;
  }

  Persona.prototype.attachLogin = function(login_btn_id) {
    // check if the login button exists
    if($('#'+login_btn_id).length) {
      $('#'+login_btn_id).click(function(e) {
        e.preventDefault();
        navigator.id.request();
      });
    }
  };

  Persona.prototype.attachLogout = function(logout_btn_id) {
    // check if the logout button exists
    if($('#'+logout_btn_id).length) {
      $('#'+logout_btn_id).click(function(e) {
        e.preventDefault();
        navigator.id.logout();
      });
    }
  };

  M.init = function() {
    var persona = new Persona(M.loggedInUser(), M.loginURL(), M.logoutURL());
    console.log(persona);
    persona.attachLogin('login');
    persona.attachLogout('logout');
  };

})(window.M);
